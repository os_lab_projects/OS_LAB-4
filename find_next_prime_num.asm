
_find_next_prime_num:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "user.h"
#include "stat.h"

int main(int argc, char * argv[]) {
   0:	f3 0f 1e fb          	endbr32 
   4:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   8:	83 e4 f0             	and    $0xfffffff0,%esp
   b:	ff 71 fc             	pushl  -0x4(%ecx)
   e:	55                   	push   %ebp
   f:	89 e5                	mov    %esp,%ebp
  11:	56                   	push   %esi
  12:	53                   	push   %ebx
  13:	51                   	push   %ecx
  14:	81 ec 0c 04 00 00    	sub    $0x40c,%esp
    char buff[1024];
    int a, tempEBX;
    if(argc < 2) {
  1a:	83 39 01             	cmpl   $0x1,(%ecx)
int main(int argc, char * argv[]) {
  1d:	8b 41 04             	mov    0x4(%ecx),%eax
    if(argc < 2) {
  20:	7e 42                	jle    64 <main+0x64>
        printf(1, "enter an integer:\n");
        read(0, buff, 1024);
        a = atoi(buff);
    } else {
        a = atoi(argv[1]);
  22:	83 ec 0c             	sub    $0xc,%esp
  25:	ff 70 04             	pushl  0x4(%eax)
  28:	e8 63 02 00 00       	call   290 <atoi>
  2d:	83 c4 10             	add    $0x10,%esp
  30:	89 c3                	mov    %eax,%ebx
    }
    asm volatile(
  32:	89 de                	mov    %ebx,%esi
  34:	89 db                	mov    %ebx,%ebx
		"movl %%ebx, %0;" // saved_ebx = ebx
		"movl %1, %%ebx;" // ebx = number
		: "=r" (tempEBX)
		: "r"(a)
	);
	printf(1, "in user mode find_next_prime_num() called\n");
  36:	83 ec 08             	sub    $0x8,%esp
  39:	68 30 08 00 00       	push   $0x830
  3e:	6a 01                	push   $0x1
  40:	e8 5b 04 00 00       	call   4a0 <printf>
	printf(1, "sum of digits is %d\n" , find_next_prime_num(a));
  45:	89 1c 24             	mov    %ebx,(%esp)
  48:	e8 56 03 00 00       	call   3a3 <find_next_prime_num>
  4d:	83 c4 0c             	add    $0xc,%esp
  50:	50                   	push   %eax
  51:	68 1b 08 00 00       	push   $0x81b
  56:	6a 01                	push   $0x1
  58:	e8 43 04 00 00       	call   4a0 <printf>
	asm("movl %0, %%ebx" : : "r"(tempEBX)); // ebx = saved_ebx -> restore
  5d:	89 f3                	mov    %esi,%ebx
	exit();
  5f:	e8 9f 02 00 00       	call   303 <exit>
        printf(1, "enter an integer:\n");
  64:	50                   	push   %eax
        read(0, buff, 1024);
  65:	8d 9d e8 fb ff ff    	lea    -0x418(%ebp),%ebx
        printf(1, "enter an integer:\n");
  6b:	50                   	push   %eax
  6c:	68 08 08 00 00       	push   $0x808
  71:	6a 01                	push   $0x1
  73:	e8 28 04 00 00       	call   4a0 <printf>
        read(0, buff, 1024);
  78:	83 c4 0c             	add    $0xc,%esp
  7b:	68 00 04 00 00       	push   $0x400
  80:	53                   	push   %ebx
  81:	6a 00                	push   $0x0
  83:	e8 93 02 00 00       	call   31b <read>
        a = atoi(buff);
  88:	89 1c 24             	mov    %ebx,(%esp)
  8b:	e8 00 02 00 00       	call   290 <atoi>
  90:	83 c4 10             	add    $0x10,%esp
  93:	89 c3                	mov    %eax,%ebx
  95:	eb 9b                	jmp    32 <main+0x32>
  97:	66 90                	xchg   %ax,%ax
  99:	66 90                	xchg   %ax,%ax
  9b:	66 90                	xchg   %ax,%ax
  9d:	66 90                	xchg   %ax,%ax
  9f:	90                   	nop

000000a0 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
  a0:	f3 0f 1e fb          	endbr32 
  a4:	55                   	push   %ebp
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
  a5:	31 c0                	xor    %eax,%eax
{
  a7:	89 e5                	mov    %esp,%ebp
  a9:	53                   	push   %ebx
  aa:	8b 4d 08             	mov    0x8(%ebp),%ecx
  ad:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  while((*s++ = *t++) != 0)
  b0:	0f b6 14 03          	movzbl (%ebx,%eax,1),%edx
  b4:	88 14 01             	mov    %dl,(%ecx,%eax,1)
  b7:	83 c0 01             	add    $0x1,%eax
  ba:	84 d2                	test   %dl,%dl
  bc:	75 f2                	jne    b0 <strcpy+0x10>
    ;
  return os;
}
  be:	89 c8                	mov    %ecx,%eax
  c0:	5b                   	pop    %ebx
  c1:	5d                   	pop    %ebp
  c2:	c3                   	ret    
  c3:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

000000d0 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  d0:	f3 0f 1e fb          	endbr32 
  d4:	55                   	push   %ebp
  d5:	89 e5                	mov    %esp,%ebp
  d7:	53                   	push   %ebx
  d8:	8b 4d 08             	mov    0x8(%ebp),%ecx
  db:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
  de:	0f b6 01             	movzbl (%ecx),%eax
  e1:	0f b6 1a             	movzbl (%edx),%ebx
  e4:	84 c0                	test   %al,%al
  e6:	75 19                	jne    101 <strcmp+0x31>
  e8:	eb 26                	jmp    110 <strcmp+0x40>
  ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  f0:	0f b6 41 01          	movzbl 0x1(%ecx),%eax
    p++, q++;
  f4:	83 c1 01             	add    $0x1,%ecx
  f7:	83 c2 01             	add    $0x1,%edx
  while(*p && *p == *q)
  fa:	0f b6 1a             	movzbl (%edx),%ebx
  fd:	84 c0                	test   %al,%al
  ff:	74 0f                	je     110 <strcmp+0x40>
 101:	38 d8                	cmp    %bl,%al
 103:	74 eb                	je     f0 <strcmp+0x20>
  return (uchar)*p - (uchar)*q;
 105:	29 d8                	sub    %ebx,%eax
}
 107:	5b                   	pop    %ebx
 108:	5d                   	pop    %ebp
 109:	c3                   	ret    
 10a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
 110:	31 c0                	xor    %eax,%eax
  return (uchar)*p - (uchar)*q;
 112:	29 d8                	sub    %ebx,%eax
}
 114:	5b                   	pop    %ebx
 115:	5d                   	pop    %ebp
 116:	c3                   	ret    
 117:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 11e:	66 90                	xchg   %ax,%ax

00000120 <strlen>:

uint
strlen(const char *s)
{
 120:	f3 0f 1e fb          	endbr32 
 124:	55                   	push   %ebp
 125:	89 e5                	mov    %esp,%ebp
 127:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  for(n = 0; s[n]; n++)
 12a:	80 3a 00             	cmpb   $0x0,(%edx)
 12d:	74 21                	je     150 <strlen+0x30>
 12f:	31 c0                	xor    %eax,%eax
 131:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 138:	83 c0 01             	add    $0x1,%eax
 13b:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
 13f:	89 c1                	mov    %eax,%ecx
 141:	75 f5                	jne    138 <strlen+0x18>
    ;
  return n;
}
 143:	89 c8                	mov    %ecx,%eax
 145:	5d                   	pop    %ebp
 146:	c3                   	ret    
 147:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 14e:	66 90                	xchg   %ax,%ax
  for(n = 0; s[n]; n++)
 150:	31 c9                	xor    %ecx,%ecx
}
 152:	5d                   	pop    %ebp
 153:	89 c8                	mov    %ecx,%eax
 155:	c3                   	ret    
 156:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 15d:	8d 76 00             	lea    0x0(%esi),%esi

00000160 <memset>:

void*
memset(void *dst, int c, uint n)
{
 160:	f3 0f 1e fb          	endbr32 
 164:	55                   	push   %ebp
 165:	89 e5                	mov    %esp,%ebp
 167:	57                   	push   %edi
 168:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
 16b:	8b 4d 10             	mov    0x10(%ebp),%ecx
 16e:	8b 45 0c             	mov    0xc(%ebp),%eax
 171:	89 d7                	mov    %edx,%edi
 173:	fc                   	cld    
 174:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
 176:	89 d0                	mov    %edx,%eax
 178:	5f                   	pop    %edi
 179:	5d                   	pop    %ebp
 17a:	c3                   	ret    
 17b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 17f:	90                   	nop

00000180 <strchr>:

char*
strchr(const char *s, char c)
{
 180:	f3 0f 1e fb          	endbr32 
 184:	55                   	push   %ebp
 185:	89 e5                	mov    %esp,%ebp
 187:	8b 45 08             	mov    0x8(%ebp),%eax
 18a:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
 18e:	0f b6 10             	movzbl (%eax),%edx
 191:	84 d2                	test   %dl,%dl
 193:	75 16                	jne    1ab <strchr+0x2b>
 195:	eb 21                	jmp    1b8 <strchr+0x38>
 197:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 19e:	66 90                	xchg   %ax,%ax
 1a0:	0f b6 50 01          	movzbl 0x1(%eax),%edx
 1a4:	83 c0 01             	add    $0x1,%eax
 1a7:	84 d2                	test   %dl,%dl
 1a9:	74 0d                	je     1b8 <strchr+0x38>
    if(*s == c)
 1ab:	38 d1                	cmp    %dl,%cl
 1ad:	75 f1                	jne    1a0 <strchr+0x20>
      return (char*)s;
  return 0;
}
 1af:	5d                   	pop    %ebp
 1b0:	c3                   	ret    
 1b1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  return 0;
 1b8:	31 c0                	xor    %eax,%eax
}
 1ba:	5d                   	pop    %ebp
 1bb:	c3                   	ret    
 1bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

000001c0 <gets>:

char*
gets(char *buf, int max)
{
 1c0:	f3 0f 1e fb          	endbr32 
 1c4:	55                   	push   %ebp
 1c5:	89 e5                	mov    %esp,%ebp
 1c7:	57                   	push   %edi
 1c8:	56                   	push   %esi
  int i, cc;
  char c;
  
  for(i=0; i+1 < max; ){
 1c9:	31 f6                	xor    %esi,%esi
{
 1cb:	53                   	push   %ebx
 1cc:	89 f3                	mov    %esi,%ebx
 1ce:	83 ec 1c             	sub    $0x1c,%esp
 1d1:	8b 7d 08             	mov    0x8(%ebp),%edi
  for(i=0; i+1 < max; ){
 1d4:	eb 33                	jmp    209 <gets+0x49>
 1d6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 1dd:	8d 76 00             	lea    0x0(%esi),%esi
    cc = read(0, &c, 1);
 1e0:	83 ec 04             	sub    $0x4,%esp
 1e3:	8d 45 e7             	lea    -0x19(%ebp),%eax
 1e6:	6a 01                	push   $0x1
 1e8:	50                   	push   %eax
 1e9:	6a 00                	push   $0x0
 1eb:	e8 2b 01 00 00       	call   31b <read>
    if(cc < 1)
 1f0:	83 c4 10             	add    $0x10,%esp
 1f3:	85 c0                	test   %eax,%eax
 1f5:	7e 1c                	jle    213 <gets+0x53>
      break;
    buf[i++] = c;
 1f7:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 1fb:	83 c7 01             	add    $0x1,%edi
 1fe:	88 47 ff             	mov    %al,-0x1(%edi)
    if(c == '\n' || c == '\r')
 201:	3c 0a                	cmp    $0xa,%al
 203:	74 23                	je     228 <gets+0x68>
 205:	3c 0d                	cmp    $0xd,%al
 207:	74 1f                	je     228 <gets+0x68>
  for(i=0; i+1 < max; ){
 209:	83 c3 01             	add    $0x1,%ebx
 20c:	89 fe                	mov    %edi,%esi
 20e:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
 211:	7c cd                	jl     1e0 <gets+0x20>
 213:	89 f3                	mov    %esi,%ebx
      break;
  }
  buf[i] = '\0';
  return buf;
}
 215:	8b 45 08             	mov    0x8(%ebp),%eax
  buf[i] = '\0';
 218:	c6 03 00             	movb   $0x0,(%ebx)
}
 21b:	8d 65 f4             	lea    -0xc(%ebp),%esp
 21e:	5b                   	pop    %ebx
 21f:	5e                   	pop    %esi
 220:	5f                   	pop    %edi
 221:	5d                   	pop    %ebp
 222:	c3                   	ret    
 223:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 227:	90                   	nop
 228:	8b 75 08             	mov    0x8(%ebp),%esi
 22b:	8b 45 08             	mov    0x8(%ebp),%eax
 22e:	01 de                	add    %ebx,%esi
 230:	89 f3                	mov    %esi,%ebx
  buf[i] = '\0';
 232:	c6 03 00             	movb   $0x0,(%ebx)
}
 235:	8d 65 f4             	lea    -0xc(%ebp),%esp
 238:	5b                   	pop    %ebx
 239:	5e                   	pop    %esi
 23a:	5f                   	pop    %edi
 23b:	5d                   	pop    %ebp
 23c:	c3                   	ret    
 23d:	8d 76 00             	lea    0x0(%esi),%esi

00000240 <stat>:

int
stat(const char *n, struct stat *st)
{
 240:	f3 0f 1e fb          	endbr32 
 244:	55                   	push   %ebp
 245:	89 e5                	mov    %esp,%ebp
 247:	56                   	push   %esi
 248:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 249:	83 ec 08             	sub    $0x8,%esp
 24c:	6a 00                	push   $0x0
 24e:	ff 75 08             	pushl  0x8(%ebp)
 251:	e8 ed 00 00 00       	call   343 <open>
  if(fd < 0)
 256:	83 c4 10             	add    $0x10,%esp
 259:	85 c0                	test   %eax,%eax
 25b:	78 2b                	js     288 <stat+0x48>
    return -1;
  r = fstat(fd, st);
 25d:	83 ec 08             	sub    $0x8,%esp
 260:	ff 75 0c             	pushl  0xc(%ebp)
 263:	89 c3                	mov    %eax,%ebx
 265:	50                   	push   %eax
 266:	e8 f0 00 00 00       	call   35b <fstat>
  close(fd);
 26b:	89 1c 24             	mov    %ebx,(%esp)
  r = fstat(fd, st);
 26e:	89 c6                	mov    %eax,%esi
  close(fd);
 270:	e8 b6 00 00 00       	call   32b <close>
  return r;
 275:	83 c4 10             	add    $0x10,%esp
}
 278:	8d 65 f8             	lea    -0x8(%ebp),%esp
 27b:	89 f0                	mov    %esi,%eax
 27d:	5b                   	pop    %ebx
 27e:	5e                   	pop    %esi
 27f:	5d                   	pop    %ebp
 280:	c3                   	ret    
 281:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
 288:	be ff ff ff ff       	mov    $0xffffffff,%esi
 28d:	eb e9                	jmp    278 <stat+0x38>
 28f:	90                   	nop

00000290 <atoi>:

int
atoi(const char *s)
{
 290:	f3 0f 1e fb          	endbr32 
 294:	55                   	push   %ebp
 295:	89 e5                	mov    %esp,%ebp
 297:	53                   	push   %ebx
 298:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 29b:	0f be 02             	movsbl (%edx),%eax
 29e:	8d 48 d0             	lea    -0x30(%eax),%ecx
 2a1:	80 f9 09             	cmp    $0x9,%cl
  n = 0;
 2a4:	b9 00 00 00 00       	mov    $0x0,%ecx
  while('0' <= *s && *s <= '9')
 2a9:	77 1a                	ja     2c5 <atoi+0x35>
 2ab:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 2af:	90                   	nop
    n = n*10 + *s++ - '0';
 2b0:	83 c2 01             	add    $0x1,%edx
 2b3:	8d 0c 89             	lea    (%ecx,%ecx,4),%ecx
 2b6:	8d 4c 48 d0          	lea    -0x30(%eax,%ecx,2),%ecx
  while('0' <= *s && *s <= '9')
 2ba:	0f be 02             	movsbl (%edx),%eax
 2bd:	8d 58 d0             	lea    -0x30(%eax),%ebx
 2c0:	80 fb 09             	cmp    $0x9,%bl
 2c3:	76 eb                	jbe    2b0 <atoi+0x20>
  return n;
}
 2c5:	89 c8                	mov    %ecx,%eax
 2c7:	5b                   	pop    %ebx
 2c8:	5d                   	pop    %ebp
 2c9:	c3                   	ret    
 2ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

000002d0 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 2d0:	f3 0f 1e fb          	endbr32 
 2d4:	55                   	push   %ebp
 2d5:	89 e5                	mov    %esp,%ebp
 2d7:	57                   	push   %edi
 2d8:	8b 45 10             	mov    0x10(%ebp),%eax
 2db:	8b 55 08             	mov    0x8(%ebp),%edx
 2de:	56                   	push   %esi
 2df:	8b 75 0c             	mov    0xc(%ebp),%esi
  char *dst;
  const char *src;

  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 2e2:	85 c0                	test   %eax,%eax
 2e4:	7e 0f                	jle    2f5 <memmove+0x25>
 2e6:	01 d0                	add    %edx,%eax
  dst = vdst;
 2e8:	89 d7                	mov    %edx,%edi
 2ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    *dst++ = *src++;
 2f0:	a4                   	movsb  %ds:(%esi),%es:(%edi)
  while(n-- > 0)
 2f1:	39 f8                	cmp    %edi,%eax
 2f3:	75 fb                	jne    2f0 <memmove+0x20>
  return vdst;
}
 2f5:	5e                   	pop    %esi
 2f6:	89 d0                	mov    %edx,%eax
 2f8:	5f                   	pop    %edi
 2f9:	5d                   	pop    %ebp
 2fa:	c3                   	ret    

000002fb <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 2fb:	b8 01 00 00 00       	mov    $0x1,%eax
 300:	cd 40                	int    $0x40
 302:	c3                   	ret    

00000303 <exit>:
SYSCALL(exit)
 303:	b8 02 00 00 00       	mov    $0x2,%eax
 308:	cd 40                	int    $0x40
 30a:	c3                   	ret    

0000030b <wait>:
SYSCALL(wait)
 30b:	b8 03 00 00 00       	mov    $0x3,%eax
 310:	cd 40                	int    $0x40
 312:	c3                   	ret    

00000313 <pipe>:
SYSCALL(pipe)
 313:	b8 04 00 00 00       	mov    $0x4,%eax
 318:	cd 40                	int    $0x40
 31a:	c3                   	ret    

0000031b <read>:
SYSCALL(read)
 31b:	b8 05 00 00 00       	mov    $0x5,%eax
 320:	cd 40                	int    $0x40
 322:	c3                   	ret    

00000323 <write>:
SYSCALL(write)
 323:	b8 10 00 00 00       	mov    $0x10,%eax
 328:	cd 40                	int    $0x40
 32a:	c3                   	ret    

0000032b <close>:
SYSCALL(close)
 32b:	b8 15 00 00 00       	mov    $0x15,%eax
 330:	cd 40                	int    $0x40
 332:	c3                   	ret    

00000333 <kill>:
SYSCALL(kill)
 333:	b8 06 00 00 00       	mov    $0x6,%eax
 338:	cd 40                	int    $0x40
 33a:	c3                   	ret    

0000033b <exec>:
SYSCALL(exec)
 33b:	b8 07 00 00 00       	mov    $0x7,%eax
 340:	cd 40                	int    $0x40
 342:	c3                   	ret    

00000343 <open>:
SYSCALL(open)
 343:	b8 0f 00 00 00       	mov    $0xf,%eax
 348:	cd 40                	int    $0x40
 34a:	c3                   	ret    

0000034b <mknod>:
SYSCALL(mknod)
 34b:	b8 11 00 00 00       	mov    $0x11,%eax
 350:	cd 40                	int    $0x40
 352:	c3                   	ret    

00000353 <unlink>:
SYSCALL(unlink)
 353:	b8 12 00 00 00       	mov    $0x12,%eax
 358:	cd 40                	int    $0x40
 35a:	c3                   	ret    

0000035b <fstat>:
SYSCALL(fstat)
 35b:	b8 08 00 00 00       	mov    $0x8,%eax
 360:	cd 40                	int    $0x40
 362:	c3                   	ret    

00000363 <link>:
SYSCALL(link)
 363:	b8 13 00 00 00       	mov    $0x13,%eax
 368:	cd 40                	int    $0x40
 36a:	c3                   	ret    

0000036b <mkdir>:
SYSCALL(mkdir)
 36b:	b8 14 00 00 00       	mov    $0x14,%eax
 370:	cd 40                	int    $0x40
 372:	c3                   	ret    

00000373 <chdir>:
SYSCALL(chdir)
 373:	b8 09 00 00 00       	mov    $0x9,%eax
 378:	cd 40                	int    $0x40
 37a:	c3                   	ret    

0000037b <dup>:
SYSCALL(dup)
 37b:	b8 0a 00 00 00       	mov    $0xa,%eax
 380:	cd 40                	int    $0x40
 382:	c3                   	ret    

00000383 <getpid>:
SYSCALL(getpid)
 383:	b8 0b 00 00 00       	mov    $0xb,%eax
 388:	cd 40                	int    $0x40
 38a:	c3                   	ret    

0000038b <sbrk>:
SYSCALL(sbrk)
 38b:	b8 0c 00 00 00       	mov    $0xc,%eax
 390:	cd 40                	int    $0x40
 392:	c3                   	ret    

00000393 <sleep>:
SYSCALL(sleep)
 393:	b8 0d 00 00 00       	mov    $0xd,%eax
 398:	cd 40                	int    $0x40
 39a:	c3                   	ret    

0000039b <uptime>:
SYSCALL(uptime)
 39b:	b8 0e 00 00 00       	mov    $0xe,%eax
 3a0:	cd 40                	int    $0x40
 3a2:	c3                   	ret    

000003a3 <find_next_prime_num>:
SYSCALL(find_next_prime_num)
 3a3:	b8 16 00 00 00       	mov    $0x16,%eax
 3a8:	cd 40                	int    $0x40
 3aa:	c3                   	ret    

000003ab <get_call_count>:
SYSCALL(get_call_count)
 3ab:	b8 17 00 00 00       	mov    $0x17,%eax
 3b0:	cd 40                	int    $0x40
 3b2:	c3                   	ret    

000003b3 <get_most_caller>:
SYSCALL(get_most_caller)
 3b3:	b8 18 00 00 00       	mov    $0x18,%eax
 3b8:	cd 40                	int    $0x40
 3ba:	c3                   	ret    

000003bb <wait_for_process>:
SYSCALL(wait_for_process)
 3bb:	b8 19 00 00 00       	mov    $0x19,%eax
 3c0:	cd 40                	int    $0x40
 3c2:	c3                   	ret    

000003c3 <sem_init>:
SYSCALL(sem_init)
 3c3:	b8 1a 00 00 00       	mov    $0x1a,%eax
 3c8:	cd 40                	int    $0x40
 3ca:	c3                   	ret    

000003cb <sem_acquire>:
SYSCALL(sem_acquire)
 3cb:	b8 1b 00 00 00       	mov    $0x1b,%eax
 3d0:	cd 40                	int    $0x40
 3d2:	c3                   	ret    

000003d3 <sem_release>:
SYSCALL(sem_release)
 3d3:	b8 1c 00 00 00       	mov    $0x1c,%eax
 3d8:	cd 40                	int    $0x40
 3da:	c3                   	ret    

000003db <multiple_acquire>:
SYSCALL(multiple_acquire)
 3db:	b8 1d 00 00 00       	mov    $0x1d,%eax
 3e0:	cd 40                	int    $0x40
 3e2:	c3                   	ret    
 3e3:	66 90                	xchg   %ax,%ax
 3e5:	66 90                	xchg   %ax,%ax
 3e7:	66 90                	xchg   %ax,%ax
 3e9:	66 90                	xchg   %ax,%ax
 3eb:	66 90                	xchg   %ax,%ax
 3ed:	66 90                	xchg   %ax,%ax
 3ef:	90                   	nop

000003f0 <printint>:
  write(fd, &c, 1);
}

static void
printint(int fd, int xx, int base, int sgn)
{
 3f0:	55                   	push   %ebp
 3f1:	89 e5                	mov    %esp,%ebp
 3f3:	57                   	push   %edi
 3f4:	56                   	push   %esi
 3f5:	53                   	push   %ebx
 3f6:	83 ec 3c             	sub    $0x3c,%esp
 3f9:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
  uint x;

  neg = 0;
  if(sgn && xx < 0){
    neg = 1;
    x = -xx;
 3fc:	89 d1                	mov    %edx,%ecx
{
 3fe:	89 45 b8             	mov    %eax,-0x48(%ebp)
  if(sgn && xx < 0){
 401:	85 d2                	test   %edx,%edx
 403:	0f 89 7f 00 00 00    	jns    488 <printint+0x98>
 409:	f6 45 08 01          	testb  $0x1,0x8(%ebp)
 40d:	74 79                	je     488 <printint+0x98>
    neg = 1;
 40f:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%ebp)
    x = -xx;
 416:	f7 d9                	neg    %ecx
  } else {
    x = xx;
  }

  i = 0;
 418:	31 db                	xor    %ebx,%ebx
 41a:	8d 75 d7             	lea    -0x29(%ebp),%esi
 41d:	8d 76 00             	lea    0x0(%esi),%esi
  do{
    buf[i++] = digits[x % base];
 420:	89 c8                	mov    %ecx,%eax
 422:	31 d2                	xor    %edx,%edx
 424:	89 cf                	mov    %ecx,%edi
 426:	f7 75 c4             	divl   -0x3c(%ebp)
 429:	0f b6 92 64 08 00 00 	movzbl 0x864(%edx),%edx
 430:	89 45 c0             	mov    %eax,-0x40(%ebp)
 433:	89 d8                	mov    %ebx,%eax
 435:	8d 5b 01             	lea    0x1(%ebx),%ebx
  }while((x /= base) != 0);
 438:	8b 4d c0             	mov    -0x40(%ebp),%ecx
    buf[i++] = digits[x % base];
 43b:	88 14 1e             	mov    %dl,(%esi,%ebx,1)
  }while((x /= base) != 0);
 43e:	39 7d c4             	cmp    %edi,-0x3c(%ebp)
 441:	76 dd                	jbe    420 <printint+0x30>
  if(neg)
 443:	8b 4d bc             	mov    -0x44(%ebp),%ecx
 446:	85 c9                	test   %ecx,%ecx
 448:	74 0c                	je     456 <printint+0x66>
    buf[i++] = '-';
 44a:	c6 44 1d d8 2d       	movb   $0x2d,-0x28(%ebp,%ebx,1)
    buf[i++] = digits[x % base];
 44f:	89 d8                	mov    %ebx,%eax
    buf[i++] = '-';
 451:	ba 2d 00 00 00       	mov    $0x2d,%edx

  while(--i >= 0)
 456:	8b 7d b8             	mov    -0x48(%ebp),%edi
 459:	8d 5c 05 d7          	lea    -0x29(%ebp,%eax,1),%ebx
 45d:	eb 07                	jmp    466 <printint+0x76>
 45f:	90                   	nop
 460:	0f b6 13             	movzbl (%ebx),%edx
 463:	83 eb 01             	sub    $0x1,%ebx
  write(fd, &c, 1);
 466:	83 ec 04             	sub    $0x4,%esp
 469:	88 55 d7             	mov    %dl,-0x29(%ebp)
 46c:	6a 01                	push   $0x1
 46e:	56                   	push   %esi
 46f:	57                   	push   %edi
 470:	e8 ae fe ff ff       	call   323 <write>
  while(--i >= 0)
 475:	83 c4 10             	add    $0x10,%esp
 478:	39 de                	cmp    %ebx,%esi
 47a:	75 e4                	jne    460 <printint+0x70>
    putc(fd, buf[i]);
}
 47c:	8d 65 f4             	lea    -0xc(%ebp),%esp
 47f:	5b                   	pop    %ebx
 480:	5e                   	pop    %esi
 481:	5f                   	pop    %edi
 482:	5d                   	pop    %ebp
 483:	c3                   	ret    
 484:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  neg = 0;
 488:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%ebp)
 48f:	eb 87                	jmp    418 <printint+0x28>
 491:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 498:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 49f:	90                   	nop

000004a0 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, const char *fmt, ...)
{
 4a0:	f3 0f 1e fb          	endbr32 
 4a4:	55                   	push   %ebp
 4a5:	89 e5                	mov    %esp,%ebp
 4a7:	57                   	push   %edi
 4a8:	56                   	push   %esi
 4a9:	53                   	push   %ebx
 4aa:	83 ec 2c             	sub    $0x2c,%esp
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 4ad:	8b 75 0c             	mov    0xc(%ebp),%esi
 4b0:	0f b6 1e             	movzbl (%esi),%ebx
 4b3:	84 db                	test   %bl,%bl
 4b5:	0f 84 b4 00 00 00    	je     56f <printf+0xcf>
  ap = (uint*)(void*)&fmt + 1;
 4bb:	8d 45 10             	lea    0x10(%ebp),%eax
 4be:	83 c6 01             	add    $0x1,%esi
  write(fd, &c, 1);
 4c1:	8d 7d e7             	lea    -0x19(%ebp),%edi
  state = 0;
 4c4:	31 d2                	xor    %edx,%edx
  ap = (uint*)(void*)&fmt + 1;
 4c6:	89 45 d0             	mov    %eax,-0x30(%ebp)
 4c9:	eb 33                	jmp    4fe <printf+0x5e>
 4cb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 4cf:	90                   	nop
 4d0:	89 55 d4             	mov    %edx,-0x2c(%ebp)
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
 4d3:	ba 25 00 00 00       	mov    $0x25,%edx
      if(c == '%'){
 4d8:	83 f8 25             	cmp    $0x25,%eax
 4db:	74 17                	je     4f4 <printf+0x54>
  write(fd, &c, 1);
 4dd:	83 ec 04             	sub    $0x4,%esp
 4e0:	88 5d e7             	mov    %bl,-0x19(%ebp)
 4e3:	6a 01                	push   $0x1
 4e5:	57                   	push   %edi
 4e6:	ff 75 08             	pushl  0x8(%ebp)
 4e9:	e8 35 fe ff ff       	call   323 <write>
 4ee:	8b 55 d4             	mov    -0x2c(%ebp),%edx
      } else {
        putc(fd, c);
 4f1:	83 c4 10             	add    $0x10,%esp
  for(i = 0; fmt[i]; i++){
 4f4:	0f b6 1e             	movzbl (%esi),%ebx
 4f7:	83 c6 01             	add    $0x1,%esi
 4fa:	84 db                	test   %bl,%bl
 4fc:	74 71                	je     56f <printf+0xcf>
    c = fmt[i] & 0xff;
 4fe:	0f be cb             	movsbl %bl,%ecx
 501:	0f b6 c3             	movzbl %bl,%eax
    if(state == 0){
 504:	85 d2                	test   %edx,%edx
 506:	74 c8                	je     4d0 <printf+0x30>
      }
    } else if(state == '%'){
 508:	83 fa 25             	cmp    $0x25,%edx
 50b:	75 e7                	jne    4f4 <printf+0x54>
      if(c == 'd'){
 50d:	83 f8 64             	cmp    $0x64,%eax
 510:	0f 84 9a 00 00 00    	je     5b0 <printf+0x110>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 516:	81 e1 f7 00 00 00    	and    $0xf7,%ecx
 51c:	83 f9 70             	cmp    $0x70,%ecx
 51f:	74 5f                	je     580 <printf+0xe0>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 521:	83 f8 73             	cmp    $0x73,%eax
 524:	0f 84 d6 00 00 00    	je     600 <printf+0x160>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 52a:	83 f8 63             	cmp    $0x63,%eax
 52d:	0f 84 8d 00 00 00    	je     5c0 <printf+0x120>
        putc(fd, *ap);
        ap++;
      } else if(c == '%'){
 533:	83 f8 25             	cmp    $0x25,%eax
 536:	0f 84 b4 00 00 00    	je     5f0 <printf+0x150>
  write(fd, &c, 1);
 53c:	83 ec 04             	sub    $0x4,%esp
 53f:	c6 45 e7 25          	movb   $0x25,-0x19(%ebp)
 543:	6a 01                	push   $0x1
 545:	57                   	push   %edi
 546:	ff 75 08             	pushl  0x8(%ebp)
 549:	e8 d5 fd ff ff       	call   323 <write>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
        putc(fd, c);
 54e:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 551:	83 c4 0c             	add    $0xc,%esp
 554:	6a 01                	push   $0x1
 556:	83 c6 01             	add    $0x1,%esi
 559:	57                   	push   %edi
 55a:	ff 75 08             	pushl  0x8(%ebp)
 55d:	e8 c1 fd ff ff       	call   323 <write>
  for(i = 0; fmt[i]; i++){
 562:	0f b6 5e ff          	movzbl -0x1(%esi),%ebx
        putc(fd, c);
 566:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 569:	31 d2                	xor    %edx,%edx
  for(i = 0; fmt[i]; i++){
 56b:	84 db                	test   %bl,%bl
 56d:	75 8f                	jne    4fe <printf+0x5e>
    }
  }
}
 56f:	8d 65 f4             	lea    -0xc(%ebp),%esp
 572:	5b                   	pop    %ebx
 573:	5e                   	pop    %esi
 574:	5f                   	pop    %edi
 575:	5d                   	pop    %ebp
 576:	c3                   	ret    
 577:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 57e:	66 90                	xchg   %ax,%ax
        printint(fd, *ap, 16, 0);
 580:	83 ec 0c             	sub    $0xc,%esp
 583:	b9 10 00 00 00       	mov    $0x10,%ecx
 588:	6a 00                	push   $0x0
 58a:	8b 5d d0             	mov    -0x30(%ebp),%ebx
 58d:	8b 45 08             	mov    0x8(%ebp),%eax
 590:	8b 13                	mov    (%ebx),%edx
 592:	e8 59 fe ff ff       	call   3f0 <printint>
        ap++;
 597:	89 d8                	mov    %ebx,%eax
 599:	83 c4 10             	add    $0x10,%esp
      state = 0;
 59c:	31 d2                	xor    %edx,%edx
        ap++;
 59e:	83 c0 04             	add    $0x4,%eax
 5a1:	89 45 d0             	mov    %eax,-0x30(%ebp)
 5a4:	e9 4b ff ff ff       	jmp    4f4 <printf+0x54>
 5a9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
        printint(fd, *ap, 10, 1);
 5b0:	83 ec 0c             	sub    $0xc,%esp
 5b3:	b9 0a 00 00 00       	mov    $0xa,%ecx
 5b8:	6a 01                	push   $0x1
 5ba:	eb ce                	jmp    58a <printf+0xea>
 5bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        putc(fd, *ap);
 5c0:	8b 5d d0             	mov    -0x30(%ebp),%ebx
  write(fd, &c, 1);
 5c3:	83 ec 04             	sub    $0x4,%esp
        putc(fd, *ap);
 5c6:	8b 03                	mov    (%ebx),%eax
  write(fd, &c, 1);
 5c8:	6a 01                	push   $0x1
        ap++;
 5ca:	83 c3 04             	add    $0x4,%ebx
  write(fd, &c, 1);
 5cd:	57                   	push   %edi
 5ce:	ff 75 08             	pushl  0x8(%ebp)
        putc(fd, *ap);
 5d1:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 5d4:	e8 4a fd ff ff       	call   323 <write>
        ap++;
 5d9:	89 5d d0             	mov    %ebx,-0x30(%ebp)
 5dc:	83 c4 10             	add    $0x10,%esp
      state = 0;
 5df:	31 d2                	xor    %edx,%edx
 5e1:	e9 0e ff ff ff       	jmp    4f4 <printf+0x54>
 5e6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 5ed:	8d 76 00             	lea    0x0(%esi),%esi
        putc(fd, c);
 5f0:	88 5d e7             	mov    %bl,-0x19(%ebp)
  write(fd, &c, 1);
 5f3:	83 ec 04             	sub    $0x4,%esp
 5f6:	e9 59 ff ff ff       	jmp    554 <printf+0xb4>
 5fb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 5ff:	90                   	nop
        s = (char*)*ap;
 600:	8b 45 d0             	mov    -0x30(%ebp),%eax
 603:	8b 18                	mov    (%eax),%ebx
        ap++;
 605:	83 c0 04             	add    $0x4,%eax
 608:	89 45 d0             	mov    %eax,-0x30(%ebp)
        if(s == 0)
 60b:	85 db                	test   %ebx,%ebx
 60d:	74 17                	je     626 <printf+0x186>
        while(*s != 0){
 60f:	0f b6 03             	movzbl (%ebx),%eax
      state = 0;
 612:	31 d2                	xor    %edx,%edx
        while(*s != 0){
 614:	84 c0                	test   %al,%al
 616:	0f 84 d8 fe ff ff    	je     4f4 <printf+0x54>
 61c:	89 75 d4             	mov    %esi,-0x2c(%ebp)
 61f:	89 de                	mov    %ebx,%esi
 621:	8b 5d 08             	mov    0x8(%ebp),%ebx
 624:	eb 1a                	jmp    640 <printf+0x1a0>
          s = "(null)";
 626:	bb 5b 08 00 00       	mov    $0x85b,%ebx
        while(*s != 0){
 62b:	89 75 d4             	mov    %esi,-0x2c(%ebp)
 62e:	b8 28 00 00 00       	mov    $0x28,%eax
 633:	89 de                	mov    %ebx,%esi
 635:	8b 5d 08             	mov    0x8(%ebp),%ebx
 638:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 63f:	90                   	nop
  write(fd, &c, 1);
 640:	83 ec 04             	sub    $0x4,%esp
          s++;
 643:	83 c6 01             	add    $0x1,%esi
 646:	88 45 e7             	mov    %al,-0x19(%ebp)
  write(fd, &c, 1);
 649:	6a 01                	push   $0x1
 64b:	57                   	push   %edi
 64c:	53                   	push   %ebx
 64d:	e8 d1 fc ff ff       	call   323 <write>
        while(*s != 0){
 652:	0f b6 06             	movzbl (%esi),%eax
 655:	83 c4 10             	add    $0x10,%esp
 658:	84 c0                	test   %al,%al
 65a:	75 e4                	jne    640 <printf+0x1a0>
 65c:	8b 75 d4             	mov    -0x2c(%ebp),%esi
      state = 0;
 65f:	31 d2                	xor    %edx,%edx
 661:	e9 8e fe ff ff       	jmp    4f4 <printf+0x54>
 666:	66 90                	xchg   %ax,%ax
 668:	66 90                	xchg   %ax,%ax
 66a:	66 90                	xchg   %ax,%ax
 66c:	66 90                	xchg   %ax,%ax
 66e:	66 90                	xchg   %ax,%ax

00000670 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 670:	f3 0f 1e fb          	endbr32 
 674:	55                   	push   %ebp
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 675:	a1 14 0b 00 00       	mov    0xb14,%eax
{
 67a:	89 e5                	mov    %esp,%ebp
 67c:	57                   	push   %edi
 67d:	56                   	push   %esi
 67e:	53                   	push   %ebx
 67f:	8b 5d 08             	mov    0x8(%ebp),%ebx
 682:	8b 10                	mov    (%eax),%edx
  bp = (Header*)ap - 1;
 684:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 687:	39 c8                	cmp    %ecx,%eax
 689:	73 15                	jae    6a0 <free+0x30>
 68b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 68f:	90                   	nop
 690:	39 d1                	cmp    %edx,%ecx
 692:	72 14                	jb     6a8 <free+0x38>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 694:	39 d0                	cmp    %edx,%eax
 696:	73 10                	jae    6a8 <free+0x38>
{
 698:	89 d0                	mov    %edx,%eax
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 69a:	8b 10                	mov    (%eax),%edx
 69c:	39 c8                	cmp    %ecx,%eax
 69e:	72 f0                	jb     690 <free+0x20>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 6a0:	39 d0                	cmp    %edx,%eax
 6a2:	72 f4                	jb     698 <free+0x28>
 6a4:	39 d1                	cmp    %edx,%ecx
 6a6:	73 f0                	jae    698 <free+0x28>
      break;
  if(bp + bp->s.size == p->s.ptr){
 6a8:	8b 73 fc             	mov    -0x4(%ebx),%esi
 6ab:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 6ae:	39 fa                	cmp    %edi,%edx
 6b0:	74 1e                	je     6d0 <free+0x60>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 6b2:	89 53 f8             	mov    %edx,-0x8(%ebx)
  if(p + p->s.size == bp){
 6b5:	8b 50 04             	mov    0x4(%eax),%edx
 6b8:	8d 34 d0             	lea    (%eax,%edx,8),%esi
 6bb:	39 f1                	cmp    %esi,%ecx
 6bd:	74 28                	je     6e7 <free+0x77>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 6bf:	89 08                	mov    %ecx,(%eax)
  freep = p;
}
 6c1:	5b                   	pop    %ebx
  freep = p;
 6c2:	a3 14 0b 00 00       	mov    %eax,0xb14
}
 6c7:	5e                   	pop    %esi
 6c8:	5f                   	pop    %edi
 6c9:	5d                   	pop    %ebp
 6ca:	c3                   	ret    
 6cb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
 6cf:	90                   	nop
    bp->s.size += p->s.ptr->s.size;
 6d0:	03 72 04             	add    0x4(%edx),%esi
 6d3:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 6d6:	8b 10                	mov    (%eax),%edx
 6d8:	8b 12                	mov    (%edx),%edx
 6da:	89 53 f8             	mov    %edx,-0x8(%ebx)
  if(p + p->s.size == bp){
 6dd:	8b 50 04             	mov    0x4(%eax),%edx
 6e0:	8d 34 d0             	lea    (%eax,%edx,8),%esi
 6e3:	39 f1                	cmp    %esi,%ecx
 6e5:	75 d8                	jne    6bf <free+0x4f>
    p->s.size += bp->s.size;
 6e7:	03 53 fc             	add    -0x4(%ebx),%edx
  freep = p;
 6ea:	a3 14 0b 00 00       	mov    %eax,0xb14
    p->s.size += bp->s.size;
 6ef:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 6f2:	8b 53 f8             	mov    -0x8(%ebx),%edx
 6f5:	89 10                	mov    %edx,(%eax)
}
 6f7:	5b                   	pop    %ebx
 6f8:	5e                   	pop    %esi
 6f9:	5f                   	pop    %edi
 6fa:	5d                   	pop    %ebp
 6fb:	c3                   	ret    
 6fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00000700 <malloc>:
  return freep;
}

void*
malloc(uint nbytes)
{
 700:	f3 0f 1e fb          	endbr32 
 704:	55                   	push   %ebp
 705:	89 e5                	mov    %esp,%ebp
 707:	57                   	push   %edi
 708:	56                   	push   %esi
 709:	53                   	push   %ebx
 70a:	83 ec 1c             	sub    $0x1c,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 70d:	8b 45 08             	mov    0x8(%ebp),%eax
  if((prevp = freep) == 0){
 710:	8b 3d 14 0b 00 00    	mov    0xb14,%edi
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 716:	8d 70 07             	lea    0x7(%eax),%esi
 719:	c1 ee 03             	shr    $0x3,%esi
 71c:	83 c6 01             	add    $0x1,%esi
  if((prevp = freep) == 0){
 71f:	85 ff                	test   %edi,%edi
 721:	0f 84 a9 00 00 00    	je     7d0 <malloc+0xd0>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 727:	8b 07                	mov    (%edi),%eax
    if(p->s.size >= nunits){
 729:	8b 48 04             	mov    0x4(%eax),%ecx
 72c:	39 f1                	cmp    %esi,%ecx
 72e:	73 6d                	jae    79d <malloc+0x9d>
 730:	81 fe 00 10 00 00    	cmp    $0x1000,%esi
 736:	bb 00 10 00 00       	mov    $0x1000,%ebx
 73b:	0f 43 de             	cmovae %esi,%ebx
  p = sbrk(nu * sizeof(Header));
 73e:	8d 0c dd 00 00 00 00 	lea    0x0(,%ebx,8),%ecx
 745:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
 748:	eb 17                	jmp    761 <malloc+0x61>
 74a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 750:	8b 10                	mov    (%eax),%edx
    if(p->s.size >= nunits){
 752:	8b 4a 04             	mov    0x4(%edx),%ecx
 755:	39 f1                	cmp    %esi,%ecx
 757:	73 4f                	jae    7a8 <malloc+0xa8>
 759:	8b 3d 14 0b 00 00    	mov    0xb14,%edi
 75f:	89 d0                	mov    %edx,%eax
        p->s.size = nunits;
      }
      freep = prevp;
      return (void*)(p + 1);
    }
    if(p == freep)
 761:	39 c7                	cmp    %eax,%edi
 763:	75 eb                	jne    750 <malloc+0x50>
  p = sbrk(nu * sizeof(Header));
 765:	83 ec 0c             	sub    $0xc,%esp
 768:	ff 75 e4             	pushl  -0x1c(%ebp)
 76b:	e8 1b fc ff ff       	call   38b <sbrk>
  if(p == (char*)-1)
 770:	83 c4 10             	add    $0x10,%esp
 773:	83 f8 ff             	cmp    $0xffffffff,%eax
 776:	74 1b                	je     793 <malloc+0x93>
  hp->s.size = nu;
 778:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 77b:	83 ec 0c             	sub    $0xc,%esp
 77e:	83 c0 08             	add    $0x8,%eax
 781:	50                   	push   %eax
 782:	e8 e9 fe ff ff       	call   670 <free>
  return freep;
 787:	a1 14 0b 00 00       	mov    0xb14,%eax
      if((p = morecore(nunits)) == 0)
 78c:	83 c4 10             	add    $0x10,%esp
 78f:	85 c0                	test   %eax,%eax
 791:	75 bd                	jne    750 <malloc+0x50>
        return 0;
  }
}
 793:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return 0;
 796:	31 c0                	xor    %eax,%eax
}
 798:	5b                   	pop    %ebx
 799:	5e                   	pop    %esi
 79a:	5f                   	pop    %edi
 79b:	5d                   	pop    %ebp
 79c:	c3                   	ret    
    if(p->s.size >= nunits){
 79d:	89 c2                	mov    %eax,%edx
 79f:	89 f8                	mov    %edi,%eax
 7a1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(p->s.size == nunits)
 7a8:	39 ce                	cmp    %ecx,%esi
 7aa:	74 54                	je     800 <malloc+0x100>
        p->s.size -= nunits;
 7ac:	29 f1                	sub    %esi,%ecx
 7ae:	89 4a 04             	mov    %ecx,0x4(%edx)
        p += p->s.size;
 7b1:	8d 14 ca             	lea    (%edx,%ecx,8),%edx
        p->s.size = nunits;
 7b4:	89 72 04             	mov    %esi,0x4(%edx)
      freep = prevp;
 7b7:	a3 14 0b 00 00       	mov    %eax,0xb14
}
 7bc:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return (void*)(p + 1);
 7bf:	8d 42 08             	lea    0x8(%edx),%eax
}
 7c2:	5b                   	pop    %ebx
 7c3:	5e                   	pop    %esi
 7c4:	5f                   	pop    %edi
 7c5:	5d                   	pop    %ebp
 7c6:	c3                   	ret    
 7c7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
 7ce:	66 90                	xchg   %ax,%ax
    base.s.ptr = freep = prevp = &base;
 7d0:	c7 05 14 0b 00 00 18 	movl   $0xb18,0xb14
 7d7:	0b 00 00 
    base.s.size = 0;
 7da:	bf 18 0b 00 00       	mov    $0xb18,%edi
    base.s.ptr = freep = prevp = &base;
 7df:	c7 05 18 0b 00 00 18 	movl   $0xb18,0xb18
 7e6:	0b 00 00 
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 7e9:	89 f8                	mov    %edi,%eax
    base.s.size = 0;
 7eb:	c7 05 1c 0b 00 00 00 	movl   $0x0,0xb1c
 7f2:	00 00 00 
    if(p->s.size >= nunits){
 7f5:	e9 36 ff ff ff       	jmp    730 <malloc+0x30>
 7fa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        prevp->s.ptr = p->s.ptr;
 800:	8b 0a                	mov    (%edx),%ecx
 802:	89 08                	mov    %ecx,(%eax)
 804:	eb b1                	jmp    7b7 <malloc+0xb7>
